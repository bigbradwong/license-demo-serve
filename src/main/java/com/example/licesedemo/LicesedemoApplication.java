package com.example.licesedemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LicesedemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(LicesedemoApplication.class, args);
    }

}
